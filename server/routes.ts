import { Application } from 'express';
import profilesRouter from '../src/port/api/router'
export default function routes(app: Application): void {
  app.use('/api/v1/profiles', profilesRouter);
};