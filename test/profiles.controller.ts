import 'mocha';
import request from 'supertest';
import Server from '../server';
import * as assert from "assert";
import * as faker from "faker";

describe('Profile test cases', () => {

    let profileToBeCreated = createProfile();
    let profileToBeUpdated = createProfile();
    let token = "token";

    let lastId: string = "";

    it("should be able to create a profile", async () => {
        let response = await request(Server)
            .post('/api/v1/profiles')
            .set('Authorization', 'Bearer ' + token)
            .send(profileToBeCreated)
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json');
        assert.equal(201, response.statusCode);
        assert.strictEqual(profileToBeCreated.username, response.body.username);
        assert.strictEqual(profileToBeCreated.firstName, response.body.firstName);
        assert.strictEqual(profileToBeCreated.lastName, response.body.lastName);
        assert.strictEqual(profileToBeCreated.sex, response.body.sex);
        lastId = response.body.id;
    });

    it("would create many profiles", async () => {
        let profile1 = await request(Server)
            .post('/api/v1/profiles')
            .set('Authorization', 'Bearer ' + token)
            .send(createProfile());
        assert.equal(profile1.statusCode, 201);
        let profile2 = await request(Server)
            .post('/api/v1/profiles')
            .set('Authorization', 'Bearer ' + token)
            .send(createProfile());
        assert.equal(profile2.statusCode, 201);
        let profile3 = await request(Server)
            .post('/api/v1/profiles')
            .set('Authorization', 'Bearer ' + token)
            .send(createProfile());
        assert.equal(profile3.statusCode, 201);
        let profile4 = await request(Server)
            .post('/api/v1/profiles')
            .set('Authorization', 'Bearer ' + token)
            .send(createProfile());
        assert.equal(profile4.statusCode, 201);
        let profile5 = await request(Server)
            .post('/api/v1/profiles')
            .set('Authorization', 'Bearer ' + token)
            .send(createProfile());
        assert.equal(profile5.statusCode, 201);
    });
    it("should be able to fetch all profiles", async () => {
            let response = await request(Server)
                .get('/api/v1/profiles')
                .set('Authorization', 'Bearer ' + token)
                .set('Content-Type', 'application/json')
                .set('Accept', 'application/json');
            assert.equal(response.statusCode, 200);
            assert.equal(response.body.length, 6);
            assert.strictEqual(profileToBeCreated.username, response.body[0].username);
            assert.strictEqual(profileToBeCreated.firstName, response.body[0].firstName);
            assert.strictEqual(profileToBeCreated.lastName, response.body[0].lastName);
            assert.strictEqual(profileToBeCreated.sex, response.body[0].sex);
        }
    );

    it("should be able to fetch a profile", async () => {
            let response = await request(Server)
                .get('/api/v1/profiles/' + lastId)
                .set('Authorization', 'Bearer ' + token)
                .set('Content-Type', 'application/json')
                .set('Accept', 'application/json');
            assert.equal(response.statusCode, 200);
            assert.strictEqual(profileToBeCreated.username, response.body.username);
            assert.strictEqual(profileToBeCreated.firstName, response.body.firstName);
            assert.strictEqual(profileToBeCreated.lastName, response.body.lastName);
            assert.strictEqual(profileToBeCreated.sex, response.body.sex);
        }
    );

    it("should be able to update a profile", async () => {
            let response = await request(Server)
                .put('/api/v1/profiles/' + lastId)
                .set('Authorization', 'Bearer ' + token)
                .set('Content-Type', 'application/json')
                .set('Accept', 'application/json')
                .send(profileToBeUpdated);
            assert.equal(response.statusCode, 200);
            assert.strictEqual(profileToBeUpdated.username, response.body.username);
            assert.strictEqual(profileToBeUpdated.firstName, response.body.firstName);
            assert.strictEqual(profileToBeUpdated.lastName, response.body.lastName);
            assert.strictEqual(profileToBeUpdated.sex, response.body.sex);
        }
    );

    it("check if the profile was updated", async () => {
            let response = await request(Server)
                .get('/api/v1/profiles/' + lastId)
                .set('Authorization', 'Bearer ' + token)
                .set('Content-Type', 'application/json')
                .set('Accept', 'application/json');
            assert.equal(response.statusCode, 200);
            assert.strictEqual(profileToBeUpdated.username, response.body.username);
            assert.strictEqual(profileToBeUpdated.firstName, response.body.firstName);
            assert.strictEqual(profileToBeUpdated.lastName, response.body.lastName);
            assert.strictEqual(profileToBeUpdated.sex, response.body.sex);
        }
    );

    it("should be able to delete a profile", async () => {
            let response = await request(Server)
                .get('/api/v1/profiles')
                .set('Authorization', 'Bearer ' + token)
                .set('Content-Type', 'application/json')
                .set('Accept', 'application/json');
            assert.equal(response.statusCode, 200);
            assert.equal(response.body.length, 6);
            let profileToDelete = response.body[3];
            let deletedProfile = await request(Server)
                .delete('/api/v1/profiles/' + profileToDelete.id)
                .set('Authorization', 'Bearer ' + token)
                .set('Content-Type', 'application/json')
                .set('Accept', 'application/json');
            assert.equal(deletedProfile.statusCode, 200);
        }
    );

    it("should fetch less profiles after the profile was deleted", async () => {
        let response = await request(Server)
            .get('/api/v1/profiles')
            .set('Authorization', 'Bearer ' + token)
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json');
        assert.equal(response.statusCode, 200);
        assert.equal(response.body.length, 5);
    });

    function createProfile() {
        return {
            username: faker.name.firstName(),
            firstName: faker.name.firstName(),
            lastName: faker.name.lastName(),
            sex: "F",
            age: faker.random.number({max: 70, min: 25, precision: 0}),
            spokenLanguages: [
                "fr"
            ],
            bio: faker.random.words(5)
        };
    }
});
