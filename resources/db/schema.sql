CREATE TABLE `profiles` (
	`aId` INT(11) NOT NULL AUTO_INCREMENT,
	`id` varchar(100) NOT NULL,
	`username` varchar(100) NOT NULL,
	`firstName` varchar(100) NOT NULL,
	`lastName` varchar(100) NOT NULL,
	`spokenLanguages` json NOT NULL,
	`sex` varchar(2) NOT NULL,
	`age` tinyint(3) NOT NULL,
	`bio` varchar(250) NULL DEFAULT NULL,
	PRIMARY KEY (`aId`),
	UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=UTF8MB4;