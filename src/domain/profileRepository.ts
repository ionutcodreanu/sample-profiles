import {Profile} from "./profile";

export interface ProfileRepository {
    create(profile: Profile): Promise<Profile>;

    all(): Promise<Profile[]>;

    byId(id: string): Promise<Profile>;

    update(profile: Profile): Promise<Profile>;

    delete(profile: Profile): Promise<void>;
}