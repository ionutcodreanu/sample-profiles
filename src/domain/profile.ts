export class Profile {
    private _username: string;
    private _firstName: string;
    private _lastName: string;
    private _sex: string;
    private _age: number;
    private _spokenLanguages: string[];
    private _bio: string;
    private _id: string;

    constructor(
        username: string,
        firstName: string,
        lastName: string,
        sex: string,
        age: number,
        spokenLanguages: string[],
        bio: string
    ) {
        this._sex = sex;
        this._age = age;
        this._spokenLanguages = spokenLanguages;
        this._bio = bio;
        this._username = username;
        this._firstName = firstName;
        this._lastName = lastName;
    }

    set id(value: string) {
        this._id = value;
    }

    get id(): string {
        return this._id;
    }

    get username(): string {
        return this._username;
    }

    get firstName(): string {
        return this._firstName;
    }

    get lastName(): string {
        return this._lastName;
    }

    get sex(): string {
        return this._sex;
    }

    get age(): number {
        return this._age;
    }

    get spokenLanguages(): string[] {
        return this._spokenLanguages;
    }

    get bio(): string {
        return this._bio;
    }

    set username(value: string) {
        this._username = value;
    }

    set firstName(value: string) {
        this._firstName = value;
    }

    set lastName(value: string) {
        this._lastName = value;
    }

    set sex(value: string) {
        this._sex = value;
    }

    set age(value: number) {
        this._age = value;
    }

    set spokenLanguages(value: string[]) {
        this._spokenLanguages = value;
    }

    set bio(value: string) {
        this._bio = value;
    }
}