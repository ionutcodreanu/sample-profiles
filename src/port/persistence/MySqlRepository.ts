import {Client} from "knex";
import {ProfileRepository} from "../../domain/profileRepository";
import {Profile} from "../../domain/profile";
import * as Knex from "knex";
import {inject, injectable} from "inversify";
import {TYPES} from "../types";
import {v4 as uuid} from "uuid";

@injectable()
export class MySqlRepository implements ProfileRepository {
    private static readonly tableName = "profiles";
    private dbConnection: Knex;

    constructor(@inject(TYPES.MySqlConnection) dbConnection: Knex) {
        this.dbConnection = dbConnection;
    }

    all(): Promise<Profile[]> {
        return this.dbConnection
            .select("*")
            .from<Profile>(MySqlRepository.tableName)
    }

    byId(id: string): Promise<Profile> {
        return this.dbConnection<Profile>(MySqlRepository.tableName)
            .where('id', id)
            .first();
    }

    create(profile: Profile): Promise<Profile> {
        profile.id = uuid();
        return this.dbConnection
            .into(MySqlRepository.tableName)
            .insert({
                    id: profile.id,
                    username: profile.username,
                    firstName: profile.firstName,
                    lastName: profile.lastName,
                    sex: profile.sex,
                    age: profile.age,
                    spokenLanguages: JSON.stringify(profile.spokenLanguages),
                    bio: profile.bio,
                }
            )
            .then(result => {
                return Promise.resolve(profile)
            })
            .catch(reason => {
                return Promise.reject(reason)
            });
    }

    delete(profile: Profile): Promise<void> {
        return this.dbConnection(MySqlRepository.tableName)
            .where('id', profile.id)
            .del();
    }

    update(profile: Profile): Promise<Profile> {
        return this.dbConnection(MySqlRepository.tableName)
            .where('id', profile.id)
            .update({
                id: profile.id,
                username: profile.username,
                firstName: profile.firstName,
                lastName: profile.lastName,
                sex: profile.sex,
                age: profile.age,
                spokenLanguages: JSON.stringify(profile.spokenLanguages),
                bio: profile.bio,
            })
            .then(result => {
                return Promise.resolve(profile);
            })
            .catch(reason => {
                return Promise.reject(reason);
            });
    }
}