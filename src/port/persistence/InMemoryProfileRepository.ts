import {v4 as uuid} from "uuid";
import {ProfileRepository} from "../../domain/profileRepository";
import {Profile} from "../../domain/profile";
import {injectable} from "inversify";

@injectable()
export class InMemoryProfileRepository implements ProfileRepository {
    private profiles: Profile[] = [];

    all(): Promise<Profile[]> {
        return Promise.resolve(this.profiles);
    }

    byId(id: string): Promise<Profile> {
        const profile = this.profiles.find(row => row.id == id);
        if (profile) {
            return Promise.resolve(profile);
        } else {
            return Promise.reject("Profile not found");
        }
    }

    create(profile: Profile): Promise<Profile> {
        profile.id = uuid();
        this.profiles.push(profile);
        return Promise.resolve(profile);
    }

    update(profile: Profile): Promise<Profile> {
        this.profiles.map(profileLocal => this.profiles.find(current => current.id === profile.id) || profile);
        return Promise.resolve(profile);
    }

    delete(profile: Profile): Promise<void> {
        this.profiles = this.profiles.filter(currentProfile => currentProfile.id != profile.id);
        return Promise.resolve();
    }
}