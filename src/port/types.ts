const TYPES = {
    ProfileService: Symbol.for("ProfileService"),
    ProfileRepository: Symbol.for("ProfileRepository"),
    ProfilesController: Symbol.for("ProfilesController"),
    MySqlRepository: Symbol.for("MySqlRepository"),
    MySqlConnection: Symbol.for("MySqlConnection")
};

export {TYPES};