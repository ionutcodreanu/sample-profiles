import {Request, Response} from 'express';
import {ProfileService} from "../../../application/ProfileService";
import {CreateProfileCommand} from "../../../application/CreateProfileCommand";
import {Profile} from "../../../domain/profile";
import {UpdateProfileCommand} from "../../../application/UpdateProfileCommand";
import {inject, injectable} from "inversify";
import {TYPES} from "../../types";

@injectable()
export class ProfilesController {
    private readonly profileService: ProfileService;

    constructor(@inject(TYPES.ProfileService) profileService: ProfileService) {
        this.profileService = profileService;
    }

    all(req: Request, res: Response): void {
        this.profileService.all().then(response =>
            res.status(200)
                .json(response.map(profile => this.mapProfile(profile)))
        ).catch(reason => res.status(500));
    }

    create(req: Request, res: Response): void {
        const command: CreateProfileCommand = {
            username: req.body.username,
            firstName: req.body.firstName,
            lastName: req.body.lastName,
            sex: req.body.sex,
            age: req.body.age,
            spokenLanguages: req.body.spokenLanguages,
            bio: req.body.bio,
        };
        this.profileService.create(command).then(
            response => res.status(201)
                .json(this.mapProfile(response))
        ).catch(reason => {
            console.log(reason);
            res.status(500).json("")
        });
    }

    byId(req: Request, res: Response): void {
        const profileId: string = req.params['id'];
        this.profileService.byId(profileId)
            .then(response => res.status(200).json(this.mapProfile(response)))
            .catch(reason => res.status(404).json({}));
    }

    update(req: Request, res: Response): void {
        const profileId: string = req.params['id'];
        const command: UpdateProfileCommand = {
            id: profileId,
            username: req.body.username,
            firstName: req.body.firstName,
            lastName: req.body.lastName,
            sex: req.body.sex,
            age: req.body.age,
            spokenLanguages: req.body.spokenLanguages,
            bio: req.body.bio,
        };
        this.profileService.update(command).then(
            response => res.status(200)
                .json(this.mapProfile(response))
        ).catch(reason => {
                console.log(reason);
                return res.status(404).json({});
            }
        );
    }

    delete(req: Request, res: Response): void {
        const profileId: string = req.params['id'];
        this.profileService.delete(profileId).then(
            () => res.status(200).json({})
        ).catch(reason => res.status(404).json());
    }

    mapProfile(profile: Profile): object {
        return {
            id: profile.id,
            username: profile.username,
            firstName: profile.firstName,
            lastName: profile.lastName,
            sex: profile.sex,
            age: profile.age,
            spokenLanguages: profile.spokenLanguages,
            bio: profile.bio,
        };
    }
}
