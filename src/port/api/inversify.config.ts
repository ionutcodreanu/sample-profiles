import {Container} from "inversify";
import {TYPES} from "../types";
import {ProfileService} from "../../application/ProfileService";
import {ProfileRepository} from "../../domain/profileRepository";
import {ProfilesController} from "./controllers/ProfilesController";
import Knex from "knex";
import {MySqlRepository} from "../persistence/MySqlRepository";

const appContainer = new Container();
appContainer.bind<Knex>(TYPES.MySqlConnection).toConstantValue(Knex({
    client: "mysql2",
    connection: {
        host: process.env.MYSQL_HOST,
        user: process.env.MYSQL_USER,
        password: process.env.MYSQL_PASSWORD,
        database: process.env.MYSQL_DATABASE
    },
}));
appContainer.bind<ProfileRepository>(TYPES.ProfileRepository).to(MySqlRepository);
appContainer.bind<ProfileService>(TYPES.ProfileService).to(ProfileService);
appContainer.bind<ProfilesController>(TYPES.ProfilesController).to(ProfilesController);

export {appContainer};