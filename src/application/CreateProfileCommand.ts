export type CreateProfileCommand = {
    username: string,
    firstName: string,
    lastName: string,
    sex: string,
    age: number,
    spokenLanguages: string[],
    bio: string,
}