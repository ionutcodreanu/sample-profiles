import {ProfileRepository} from "../domain/profileRepository";
import {Profile} from "../domain/profile";
import {CreateProfileCommand} from "./CreateProfileCommand";
import {UpdateProfileCommand} from "./UpdateProfileCommand";
import {TYPES} from "../port/types";
import {inject, injectable} from "inversify";

@injectable()
export class ProfileService {
    private readonly profileRepository: ProfileRepository;

    public constructor(@inject(TYPES.ProfileRepository) profileRepository: ProfileRepository) {
        this.profileRepository = profileRepository;
    }

    all(): Promise<Profile[]> {
        return this.profileRepository.all();
    }

    create(createProfileCommand: CreateProfileCommand): Promise<Profile> {
        const profile = new Profile(
            createProfileCommand.username,
            createProfileCommand.firstName,
            createProfileCommand.lastName,
            createProfileCommand.sex,
            createProfileCommand.age,
            createProfileCommand.spokenLanguages,
            createProfileCommand.bio,
        );
        return this.profileRepository.create(profile);
    }

    byId(profileId: string): Promise<Profile> {
        return this.profileRepository.byId(profileId);
    }

    update(command: UpdateProfileCommand): Promise<Profile> {
        return this.profileRepository.byId(command.id).then(
            profile => {
                profile.username = command.username;
                profile.firstName = command.firstName;
                profile.lastName = command.lastName;
                profile.sex = command.sex;
                profile.age = command.age;
                profile.spokenLanguages = command.spokenLanguages;
                profile.bio = command.bio;
                return this.profileRepository.update(profile);
            }
        );
    }

    delete(profileId: string): Promise<void> {
        return this.profileRepository.byId(profileId).then(
            profile => this.profileRepository.delete(profile)
        );
    }
}